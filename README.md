# Express & React with Typescript example
- [Node + Express + Typescript](node-ts-example)
- [React + Typescript](react-ts-example)

# Sources:
- [เนื้อหาจากบทเรียน Typescript](https://gitlab.com/fResult/typescript-slide/-/blob/master/README.md)
  - [Lesson 10 - What's Next](https://gitlab.com/fResult/typescript-slide/-/blob/master/slides/010.what-is-next-01.md)
- [บทความเรื่อง 3 ขั้นตอนการสร้าง Web Server ด้วย NodeJS + Typescrip + Express](https://medium.com/p/ba8176b22809)
