import { RequestHandler } from 'express'

import Todo from '../models/Todo'
import TodoService from '../services/todo.service'
import { formatDateTime } from '../libs/datetime'

const service = TodoService()

function TodoController() {
  const getTodos: RequestHandler = async (req, res, next) => {
    const { task }: Partial<Todo> = req.query

    if ('task' in req.query) {
      const { code, ...respBody } = await service.getTodoByTaskName(task!)
      return res.status(code).json(respBody)
    }

    const { code, ...respBody } = await service.getAllTodos()
    res.status(code).json(respBody)
  }

  const createTodo: RequestHandler = async (req, res, next) => {
    const nowFormatted = formatDateTime(new Date())
    const { task } = <Todo>req.body

    const newTodo: Todo = {
      id: String(Date.now()),
      task,
      completed: false,
      createdAt: nowFormatted,
      updatedAt: nowFormatted
    }

    const { code, ...respBody } = await service.createTodo(newTodo)
    res.status(code).json(respBody)
  }

  const updateTodo: RequestHandler<{ id: string }> = async (req, res, next) => {
    const id = req.params.id
    const todoFromReq = <Todo>req.body
    const { code, ...respBody } = await service.updateTodo(id, todoFromReq)
    res.status(code).json(respBody)
  }

  const deleteTodo: RequestHandler<{ id: string }> = async (req, res, next) => {
    const { code, ...respBody } = await service.deleteTodo(req.params.id)
    res.status(code).json(respBody)
  }

  return { getTodos, createTodo, updateTodo, deleteTodo }
}

export default TodoController
