type Todo = {
  id: string
  task: string
  completed: boolean
  createdAt?: string
  updatedAt: string
}

export default Todo
