class UnreachableError extends Error {
  constructor(val: never, message: string) {
    super(`Error ❌ ${message}`)
  }
}