import { Router } from 'express'
import TodoController from '../controllers/todo.controller'

const router = Router()
const controller = TodoController()

router.get('/', controller.getTodos)

router.post('/', controller.createTodo)

router.patch('/:id', controller.updateTodo)

router.delete('/:id', controller.deleteTodo)

export default router
