import express/*, { NextFunction, Request, Response }*/ from 'express'
import { json } from 'body-parser'
import cors from 'cors'
import todoRouter from './routes/todo.route'

const server = express()
const PORT = 8000

server.use(cors())
server.use(json())

// server.get('/messenger/:message', (req: Request, res: Response, next: NextFunction) => {
  // res.json({ message: req.params.message })
// })

server.use('/todos', todoRouter)

server.listen(PORT, () => console.log(`Server is started at port ${PORT}`))
