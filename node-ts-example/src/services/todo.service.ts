import { promises } from 'fs'
import Todo from '../models/Todo'
import { formatDateTime } from '../libs/datetime'

const { readFile, writeFile } = promises
const todosFilePath = `${__dirname}/../json-data/todos.json`

function TodoService() {
  const getAllTodos = async () => {
    try {
      const todos: Array<Todo> = JSON.parse(
        await readFile(todosFilePath, 'utf8')
      )
      return { code: 200, count: todos.length, todos }
    } catch (err) {
      return { code: 400, errorMessage: `❌ ${err.message}` }
    }
  }

  const getTodoByTaskName = async (taskName: string) => {
    try {
      const todos: Array<Todo> = JSON.parse(
        await readFile(todosFilePath, 'utf8')
      )

      const foundTodos: Array<Todo> = todos.filter((t) => {
        return t.task.toLowerCase().includes(taskName!.trim().toLowerCase())
      })
      if (!foundTodos.length || !taskName!.trim()) {
        return { code: 404, errorMessage: 'Todo not found' }
      }

      return { code: 200, count: foundTodos.length, todos: foundTodos }
    } catch (err) {
      return { code: 400, errorMessage: `❌ ${err.message}` }
    }
  }

  const createTodo = async (todo: Todo) => {
    try {
      const todos = JSON.parse(await readFile(todosFilePath, 'utf8'))
      await writeFile(todosFilePath, JSON.stringify([...todos, todo], null, 2))
      return { code: 201, message: `Created todo success`, createdTodo: todo }
    } catch (err) {
      return {
        code: 500,
        errorMessage: 'Something went wrong',
        moreInformation: `❌ ${err.message}`
      }
    }
  }

  const updateTodo = async (id: string, todoFromReq: Todo) => {
    try {
      const todos: Array<Todo> = JSON.parse(
        await readFile(todosFilePath, 'utf8')
      )
      const foundTodo: Todo | undefined = todos.find((t) => t.id === id)

      if (!foundTodo) {
        return { code: 404, errorMessage: 'Todo not found' }
      }

      delete todoFromReq.createdAt

      const updatedTodo = Object.assign(foundTodo, {
        ...todoFromReq,
        updatedAt: formatDateTime(new Date())
      })

      await writeFile(todosFilePath, JSON.stringify(todos, null, 2))
      return {
        code: 200,
        message: `Updated todo ID ${id} success`,
        updatedTodo: updatedTodo
      }
    } catch (err) {
      return {
        code: 400,
        errorMessage: 'Something went wrong',
        moreInformation: `❌ ${err.message}`
      }
    }
  }

  const deleteTodo = async (id: string) => {
    try {
      const todos: Array<Todo> = JSON.parse(
        await readFile(todosFilePath, 'utf8')
      )
      const foundTodo = todos.find((t) => id === t.id)

      if (!foundTodo) {
        return { code: 404, errorMessage: 'Todo not found' }
      }

      const remainedTodo = todos.filter((t) => t.id !== id)
      await writeFile(todosFilePath, JSON.stringify(remainedTodo, null, 2))

      return { code: 200, message: `Deleted todo ID ${id} success` }
    } catch (err) {
      return {
        code: 400,
        errorMessage: 'Something went wrong',
        moreInformation: `❌ err.message`
      }
    }
  }

  return {
    getAllTodos,
    getTodoByTaskName,
    createTodo,
    updateTodo,
    deleteTodo
  }
}

export default TodoService
