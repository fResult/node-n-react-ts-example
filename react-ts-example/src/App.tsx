import React, { Suspense, lazy } from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  RouteProps,
  RouteComponentProps,
  Redirect
} from 'react-router-dom'
import { Row } from 'antd'

import './App.css'

const TodoList = lazy(() => import('./pages/TodoList'))
const FormTodo = lazy(() => import('./pages/FormTodo'))

const routes: Array<RouteProps> = [
  { path: '/todo-list', component: TodoList },
  { path: '/add-todo', component: FormTodo },
  { path: '/edit-todo', component: FormTodo },
  { path: '/', component: TodoList }
]
const App: React.FC = (): JSX.Element | null => {
  return (
    <Suspense
      fallback={
        <Row justify="center" align="middle" style={{ height: '100vh' }}>
          <h1>Loading...</h1>
        </Row>
      }
    >
      <div className="App">
        <Router>
          <Switch>
            <>
              {routes.map(({ path, component: Component }) => {
                return (
                  Component && (
                    <Route
                      exact
                      key={path as string}
                      path={path}
                      render={(props: RouteComponentProps<{}>) => (
                        <Component {...props} />
                      )}
                    />
                  )
                )
              })}
            </>
            <Redirect to="/" />
          </Switch>
        </Router>
      </div>
    </Suspense>
  )
}

export default App
