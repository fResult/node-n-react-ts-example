import axios from 'axios'

const BASE_URL = 'http://localhost:8000'

export async function findAll(): Promise<{
  count: number
  todos: Array<Todo>
}> {
  const { data } = await axios.get(`${BASE_URL}/todos`)
  return data
}

export async function findByTaskName(
  taskName: string
): Promise<{ count: number; todos: Array<Todo> }> {
  const { data } = await axios.get(`${BASE_URL}/todos?task=${taskName}`)
  return data
}

export async function add(
  todo: Todo
): Promise<{ message: string; addedTodo: Todo }> {
  const { data } = await axios.post(`${BASE_URL}/todos`, todo)
  return data
}

export async function editById(
  todoId: string,
  todo: Todo
): Promise<{ message: string; updatedTodo: Todo }> {
  const { data } = await axios.patch(`${BASE_URL}/todos/${todoId}`, todo)
  return data
}

export async function deleteById(
  todoId: string
): Promise<{ message: string; deletedTodo: Todo }> {
  const { data } = await axios.delete(`${BASE_URL}/todos/${todoId}`)
  return data
}
