import React, { ReactNode, CSSProperties } from 'react'
import { Layout, Typography } from 'antd'

type ContentProps = {
  pageTitle: string
  children: ReactNode
  style?: CSSProperties
}

const { Content: ContentAntD } = Layout
const { Title } = Typography

function Content({ pageTitle, children, style }: ContentProps) {
  return (
    <ContentAntD style={{ height: 'calc(100vh - 64px)', ...style }}>
      <Title level={1}>{pageTitle}</Title>
      {children}
    </ContentAntD>
  )
}

export default Content
