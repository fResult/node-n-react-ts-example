import React, { CSSProperties } from 'react'
import { Layout } from 'antd'

type HeaderProps = {
  style?: CSSProperties
}

const { Header: HeaderAntD } = Layout

function Header({ style }: HeaderProps) {
  return (
    <HeaderAntD style={{ marginBottom: 20, color: '#CCFBF1', ...style }}>
      This is Header
    </HeaderAntD>
  )
}

export default Header
