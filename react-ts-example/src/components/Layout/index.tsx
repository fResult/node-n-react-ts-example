import React from 'react'
import { Layout as LayoutAntD } from 'antd'

type LayoutProps = {
  children: React.ReactNode
}

function Layout ({ children }: LayoutProps) {
  return <LayoutAntD>{children}</LayoutAntD>
}

export default Layout
