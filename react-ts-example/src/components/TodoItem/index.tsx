import React from 'react'
import { Button, List, Checkbox, Typography } from 'antd'
import { RouteComponentProps } from 'react-router-dom'
import './index.css'

type TodoItemProps = {
  id: React.Key
  todo: Todo
  history: RouteComponentProps['history']
  onEditTodo: (todoId: string, todo: Partial<Todo>) => void
  onDeleteTodo: (todoId: string) => void
}

const { Text } = Typography

const TodoItem: React.FC<TodoItemProps> = ({
  id,
  history,
  todo,
  onDeleteTodo,
  onEditTodo
}) => {
  return (
    <List.Item
      key={id}
      actions={[
        <Button
          shape="round"
          onClick={() =>
            history.push('/edit-todo', {
              mode: 'edit',
              todo: {
                id: todo.id,
                task: todo.task,
                completed: todo.completed
              },
              person: {name: 'korn', age: 18}
            })
          }
        >
          📝{/*Edit*/}
        </Button>,
        <Button shape="round" onClick={() => onDeleteTodo(todo.id)}>
          ❌{/*Delete*/}
        </Button>
      ]}
    >
      <div>
        <Checkbox
          checked={todo.completed}
          onChange={() =>
            onEditTodo(todo.id, {
              completed: !todo.completed
            })
          }
        />
        <Text style={{ marginLeft: '1.5rem', fontSize: '1.5rem' }}>
          {todo.task}
        </Text>
      </div>
    </List.Item>
  )
}

export default TodoItem
