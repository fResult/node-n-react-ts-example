import React, { LegacyRef, useEffect, useRef, useState } from 'react'
import { Button, Typography, Form, Input, Select } from 'antd'

import { useHistory, useLocation } from 'react-router-dom'

import Layout from '../components/Layout'
import Header from '../components/Layout/Header'
import Content from '../components/Layout/Content'

import * as ApiTodo from '../apis/todos.api'

const { Title, Text } = Typography

function FormTodo() {
  const [error, setError] = useState('')

  const taskNameRef = useRef/*<LegacyRef<Input> | HTMLInputElement | null>*/ <
    any
  >(null)

  const [todoForm] = Form.useForm()
  const history = useHistory()
  const location = useLocation<{ mode: string; todo: Partial<Todo> }>()
  const mode = location?.state?.mode || 'add'
  const todo = location?.state?.todo || {}

  const { Option } = Select

  useEffect(() => {
    location.state && taskNameRef.current!.focus({ cursor: 'all' })
  })

  async function handleSaveTodo(values: {
    taskName: string
    isCompleted: boolean
  }) {
    const newTodo = {
      task: values.taskName,
      completed: values.isCompleted
    } as Todo
    try {
      mode === 'edit'
        ? await ApiTodo.editById(todo.id as string, newTodo)
        : await ApiTodo.add(newTodo)
      history.push('/')
    } catch (err) {
      setError(err.message)
    }
  }

  return (
    <Layout>
      <Header />
      <Content pageTitle={`${mode === 'add' ? 'Add' : 'Edit'} Todo`}>
        {location.pathname === '/edit-todo' && !location.state ? (
          <Title level={3}>Cannot access Edit Todo Page</Title>
        ) : (
          <div>
            <Form
              form={todoForm}
              name={`${mode === 'edit' ? 'editTodo' : 'addTodo'}`}
              onFinish={handleSaveTodo}
              style={{ width: 600, margin: '0 auto' }}
            >
              <Form.Item
                label="Task Name"
                name="taskName"
                initialValue={todo.task}
              >
                <Input ref={taskNameRef} />
              </Form.Item>
              <Form.Item
                name="isCompleted"
                label="Completed ?"
                initialValue={todo.completed ? 1 : 0}
              >
                <Select
                  style={{
                    backgroundColor: todoForm.getFieldValue('isCompleted')
                      ? '#2DD4BF'
                      : '#E5E7EB'
                  }}
                  disabled={mode === 'add'}
                >
                  <Option value={1}>YES</Option>
                  <Option value={0}>NO</Option>
                </Select>
              </Form.Item>
              <Form.Item>
                <Button htmlType="submit" type="primary" size="large">
                  Save
                </Button>
              </Form.Item>
            </Form>
            <div
              style={{
                margin: '0 auto',
                width: 650,
                backgroundColor: '#DC2626'
              }}
            >
              <Text style={{ lineHeight: '2rem', color: '#EEEEEE' }}>
                {error}
              </Text>
            </div>
          </div>
        )}
      </Content>
    </Layout>
  )
}

export default FormTodo
