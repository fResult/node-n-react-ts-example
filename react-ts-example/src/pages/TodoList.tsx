import React, { useEffect, useState } from 'react'
import { Button, List, Row, Typography } from 'antd'
import { useHistory } from 'react-router-dom'

import Header from '../components/Layout/Header'
import Layout from '../components/Layout'
import Content from '../components/Layout/Content'
import * as ApiTodo from '../apis/todos.api'
import TodoItem from '../components/TodoItem'

const { Title, Text } = Typography

function TodoList() {
  const [todos, setTodos] = useState<Array<Todo>>([])
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState('')

  const history = useHistory()

  useEffect(() => {
    ;(async () => await fetchTodos())()
  }, [])

  async function fetchTodos() {
    try {
      setLoading(true)
      const resp = await ApiTodo.findAll()
      setTodos(resp.todos)
    } catch (err) {
      setError(err.message)
    } finally {
      setLoading(false)
    }
  }

  async function handleEditTodo(todoId: string, data: Partial<Todo>) {
    try {
      setLoading(true)
      await ApiTodo.editById(todoId, data as Todo)
      await fetchTodos()
    } catch (err) {
      setError(err.message)
    } finally {
      setLoading(false)
    }
  }

  async function handleDeleteTodo(todoId: string) {
    try {
      setLoading(true)
      await ApiTodo.deleteById(todoId)
      await fetchTodos()
    } catch (err) {
      setError(err.message)
    } finally {
      setLoading(false)
    }
  }

  return (
    <Layout>
      <Header />
      <Content pageTitle="Todo List">
        {error ? (
          <div
            style={{
              margin: '0 auto',
              width: 650,
              backgroundColor: '#DC2626'
            }}
          >
            <Text style={{ lineHeight: '2rem', color: '#EEEEEE' }}>
              {error}
            </Text>
          </div>
        ) : (
          <List
            loading={loading}
            size="large"
            header={
              <Row justify="space-between">
                <Title level={2}>Tasks</Title>
                <Button
                  onClick={() => history.push('/add-todo', { mode: 'add' })}
                >
                  ➕
                </Button>
              </Row>
            }
            bordered
            style={{ width: 600, margin: '0 auto' }}
          >
            {todos.map((todo) => {
              return (
                <TodoItem
                  id={todo.id}
                  key={todo.id}
                  todo={todo}
                  onDeleteTodo={handleDeleteTodo}
                  onEditTodo={handleEditTodo}
                  history={history}
                />
              )
            })}
          </List>
        )}
      </Content>
    </Layout>
  )
}

export default TodoList
